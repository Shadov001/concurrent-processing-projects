#include <omp.h>
#include <stdio.h>

int main() {
    int a=7, i , sum = 0;
    double start, stop;

    start=omp_get_wtime();
    #pragma omp parallel for reduction(+:suma)
    for(i=0; i<500; i++) {sum+=a*a;}
    stop=omp_get_wtime();
    printf("Reduction: SUMA=%d czas =%lf\n\n", sum, stop-start);


    sum=0;
    start=omp_get_wtime();
    #pragma omp parallel for
    #pragma omp_init_lock
    for(i=0; i<500; i++) {
        #pragma omp_set_lock
        sum+=a*a;
        #pragma omp_unset_lock
    }
    stop=omp_get_wtime();
    printf("Lock: SUMA=%d czas =%lf\n\n", sum, stop-start);

    sum=0;
    start=omp_get_wtime();
    #pragma omp parallel for
    for(i=0; i<500; i++) { 
        #pragma critical
        sum+=a*a;}
    stop=omp_get_wtime();
    printf("Critical: SUMA=%d czas =%lf\n\n", sum, stop-start);

    return 0;
}


