#include <omp.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

int i;
printf("static (3)\n");
#pragma omp parallel for num_threads(4) schedule(static,3)
	for(i=0;i<15;i++) {
     		printf("Thread %d i=%d\n",omp_get_thread_num(),i);
	}
printf("\n");

printf("static (default)\n");
#pragma omp parallel for num_threads(4) schedule(static)
	for(i=0;i<15;i++) {
     		printf("Thread %d i=%d\n",omp_get_thread_num(),i);
	}
printf("\n");

printf("dynamic (3)\n");
#pragma omp parallel for num_threads(4) schedule(dynamic,3)
	for(i=0;i<15;i++) {
     	printf("Thread %d i=%d\n",omp_get_thread_num(),i);
	}
printf("\n");

printf("dynamic (default)\n");
#pragma omp parallel for num_threads(4) schedule(dynamic)
	for(i=0;i<15;i++) {
	printf("Thread %d i=%d\n",omp_get_thread_num(),i);
	}

printf("\n");
 return 0;
}




