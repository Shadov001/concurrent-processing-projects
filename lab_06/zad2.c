#include <omp.h>
#include <stdio.h>
int main(int argc, char *argv[]) {

int i, a=7;
#pragma omp parallel for num_threads(7) private(a)
//#pragma omp parallel for num_threads(7) firstprivate(a)
//#pragma omp parallel for num_threads(7) shared(a)
	for(i=0;i<10;i++) {
     	printf("Thread %d a=%d\n",omp_get_thread_num(),a);
     	a++;
	}

printf("a=%d\n",a);
 return 0;
}



