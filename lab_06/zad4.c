#include <omp.h>
#include <stdio.h>

int main(int argc, char *argv[] ) {
int i;
double start, stop;

start=omp_get_wtime();
#pragma omp parallel for num_threads(4) schedule(static,3)
	for(i=0;i<10000000;i++){}
stop=omp_get_wtime();
printf("Static (3). Czas=%lf\n",stop-start);

start=omp_get_wtime();
#pragma omp parallel for num_threads(4) schedule(static)
	for(i=0;i<10000000;i++){}
stop=omp_get_wtime();
printf("Static (default). Czas=%lf\n",stop-start);

start=omp_get_wtime();
#pragma omp parallel for num_threads(4) schedule(dynamic,3)
	for(i=0;i<10000000;i++){}
stop=omp_get_wtime();
printf("Dynamic (3). Czas=%lf\n",stop-start);

start=omp_get_wtime();
#pragma omp parallel for num_threads(4) schedule(dynamic)
	for(i=0;i<10000000;i++){}
stop=omp_get_wtime();
printf("Dynamic (default). Czas=%lf\n",stop-start);

 return 0;
}


