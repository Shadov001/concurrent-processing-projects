#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define NUM_THREADS 10


pthread_t tid[NUM_THREADS];

void *kod(void *arg)
{
	int numer=(int)arg;
	fprintf(stderr,"Jestem wątek: %d\n",tid[numer]);
	fprintf(stderr,"Kończę wątek: %d\n",tid[numer]);
	return NULL;
}

int main(int argc,char *argv[])
{
	int i,status;
	for(i=0;i<NUM_THREADS;i++)
	      pthread_create(&tid[i],NULL,kod,(void *)(i));
	for(i=0;i<NUM_THREADS;i++)
	      pthread_join(tid[i], NULL);
	return 0;
}
