#define _GNU_SOURCE
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

int zmienna_wspolna=0;

#define WYMIAR 1000
#define ROZMIAR WYMIAR*WYMIAR
double a[ROZMIAR],b[ROZMIAR],c[ROZMIAR];


double czasozajmowacz() {
  int i, j, k;
  int n=WYMIAR;
  for(i=0;i<ROZMIAR;i++) a[i]=1.0*i;
  for(i=0;i<ROZMIAR;i++) b[i]=1.0*(ROZMIAR-i);
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
      c[i+n*j]=0.0;
      for(k=0;k<n;k++){
		c[i+n*j] += a[i+n*k]*b[k+n*j];
      }
    }
  }
  return(c[ROZMIAR-1]);
}

void * zadanie_watku (void * arg_wsk) {
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	printf("\twatek potomny: uniemozliwione zabicie\n");

	czasozajmowacz();

	printf("\twatek potomny: umozliwienie zabicia\n");
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	pthread_testcancel();

	zmienna_wspolna++;
	printf("\twatek potomny: zmiana wartosci zmiennej wspolnej\n");
	return(NULL);
}

int main() {
	cpu_set_t cpu1;
	pthread_t tid;
	pthread_attr_t attr;
	void *wynik;
	int i;
	struct sched_param parametr;
	parametr.sched_priority = 60;
	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 100000);
	CPU_ZERO(&cpu1);
	CPU_SET(1,&cpu1);
	pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t),&cpu1);
	pthread_setschedparam(pthread_self(), SCHED_FIFO,&parametr);
	int k;   
	pthread_t tidy[3];
	for(k=0; k<3; k++) {
		if(k == 2)
		CPU_SET( 2, &cpu1);
		printf("Watek glowny: tworzenie watku nr: %d\n",i + 1);
		pthread_create(&tidy[k], &attr, zadanie_watku, NULL);   
		pthread_detach(tidy[k]);
	}
	printf("\twatek glowny: koniec pracy, watek odlaczony pracuje dalej\n"); 
	pthread_attr_destroy(&attr);
	pthread_exit(NULL);
	return(0); 
}


