#define _GNU_SOURCE
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

#define WYMIAR 1000
#define ROZMIAR WYMIAR*WYMIAR
double a[ROZMIAR],b[ROZMIAR],c[ROZMIAR];


double czasozajmowacz() {
  int i, j, k;
  int n=WYMIAR;
  for(i=0;i<ROZMIAR;i++) a[i]=1.0*i;
  for(i=0;i<ROZMIAR;i++) b[i]=1.0*(ROZMIAR-i);
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
      c[i+n*j]=0.0;
      for(k=0;k<n;k++){
	c[i+n*j] += a[i+n*k]*b[k+n*j];
      }
    }
  }
  return(c[ROZMIAR-1]);
}

void * zadanie_watku (void * arg_wsk) {
	pthread_attr_t attr;
	void *adres;
	size_t stos;
	int id = *( (int *) arg_wsk);
	pthread_getattr_np(pthread_self(), &attr);
	pthread_attr_getstack(&attr, &adres, &stos);
	printf("Watek nr: %d\n",id);
	printf("Rozmiar stosu: %d\n", stos);
	czasozajmowacz();
	printf("\twatek potomny: zmiana wartosci zmiennej wspolnej\n");
	printf("\t koncze prace watka %d \n", id);
	return(NULL);
}

int main() {
	pthread_attr_t attr;
	srand(time(NULL)); 	  
	pthread_t tid[10]; 
    int k; 
	for(k=0; k<10; k++) {
		pthread_attr_init(&attr);
		pthread_attr_setstacksize(&attr,(int)rand()%100000);   
		pthread_create(&tid[k], &attr, zadanie_watku, &k);  
		pthread_attr_destroy(&attr);
	}  
	pthread_attr_t atrybut;  void *addr;  
	size_t rozm;   

	pthread_attr_destroy(&attr);  
	printf("\nwatek glowny: koniec pracy, odlaczony watek pracuje dalej\n\n");  
	pthread_exit(NULL);  
	return(0);
}


