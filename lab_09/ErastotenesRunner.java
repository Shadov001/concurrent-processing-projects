package zadanie3;

public class ErastotenesRunner {
    public static void main(String[] args) {
        for(int i=2; i<=10; i++){
            new ErastotenesAlgorithmImplementation(i).start();
        }

        for(int i=1; i<=100; i++){
            if(!ErastotenesAlgorithmImplementation.numbers[i])
            System.out.println(i);
        }
    }
}

class ErastotenesAlgorithmImplementation extends Thread{
    public static boolean[] numbers = new boolean[101];
    int step;

    ErastotenesAlgorithmImplementation(int step) {
        this.step = step;
    }

    @Override
    public void run(){
        eliminateNonPrimes(step);
    }

    void eliminateNonPrimes(int step){
        int firstElement = step*step;

        for(int i = firstElement; i<=100; i+=step){
            if(!numbers[i])
                numbers[i]=!numbers[i];
         }
    }
}
