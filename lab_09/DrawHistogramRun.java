package zad2;

import java.util.Random;
import java.util.*;

public class DrawHistogramRun {
    static Set<Character> processedSigns = new HashSet<Character>();
    static int n = Image.N;
    static int m = Image.M;

    public static void main(String[] args) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(!processedSigns.contains(Image.image[i][j])){
                    processedSigns.add(Image.image[i][j]);
                    new HistogramDrawer(Image.image[i][j]).start();
                }
            }
        }
    }
}

class HistogramDrawer extends Thread{
    char sign;

    HistogramDrawer(char sign){
        this.sign = sign;
    }

    @Override
    public void run(){
        int occurences = calculateOccurence();
        draw(occurences);
    }

    private int calculateOccurence() {
        int count = 0;
        int n = Image.N;
        int m = Image.M;

        for(int i=0; i<n; i++) {
            for (int j = 0; j < m; j++)
                if (Image.image[i][j] == sign) {
                    count++;
                }
        }
        return count;
    }

    private void draw(int occurences){
        long threadId = Thread.currentThread().getId();
        System.out.print("Wątek nr " + threadId + " '" + sign + "' " );
        for(int i=0; i<occurences; i++) {
            System.out.print("=");
        }
        System.out.println();
    }
}

class Image{
    static final int N = 5;
    static final int M = 5;
    static final char[][] image = new char[N][M];

    static{
        Random random = new Random();
        for(int i=0; i<N; i++) {
            for (int j = 0; j < M; j++) {
                image[i][j] = (char) (random.nextInt(8) + 33);
                System.out.print(image[i][j]);
            }
            System.out.println();
        }
    }
}