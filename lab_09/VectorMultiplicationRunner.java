package zadanie1;

import java.util.Random;

public class VectorMultiplicationRunner {
    public static final int THREADS_NUMBER = 8;


    public static void main(String[] args) {
        printInputVectors();
        calculateOutputVector();
        printOutputVector();
    }

    static void calculateOutputVector(){
        int chunk = VectorsHolder.VECTOR_SIZE/THREADS_NUMBER;

        for(int i=0; i<THREADS_NUMBER; i++){
            if(i==THREADS_NUMBER-1){
                new VectorsSumCalculator(chunk*(THREADS_NUMBER-1), VectorsHolder.VECTOR_SIZE-1).start();
            }
            else
                new VectorsSumCalculator(i*chunk, i*chunk+chunk).start();
        }
    }
    static void printInputVectors(){
        for(int i=0; i<VectorsHolder.VECTOR_SIZE-1; i++) {
            System.out.print(VectorsHolder.input1[i] + " ");
        }
        System.out.println();
        for(int i=0; i<VectorsHolder.VECTOR_SIZE-1; i++) {
            System.out.print(VectorsHolder.input2[i] + " ");
        }
        System.out.println();
    }

    static void printOutputVector(){

        for(int i=0; i<VectorsHolder.VECTOR_SIZE-1; i++)
            System.out.print(VectorsHolder.output[i] + " ");
    }


}

class VectorsSumCalculator extends Thread{
    int startIndex;
    int endIndex;

    public VectorsSumCalculator(int startIndex, int endIndex){
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public void run(){
        for(int i = startIndex; i<=endIndex; i++){
            VectorsHolder.output[i] = VectorsHolder.input1[i] + VectorsHolder.input2[i];
        }

    }

}

class VectorsHolder{
    public static final int VECTOR_SIZE = 100;
    public static final int[] input1 = new int[100];
    public static final int[] input2 = new int[100];
    public static final int[] output = new int[100];

    static{
        Random random = new Random();
        for(int i=0; i<100; i++){
            input1[i] = random.nextInt(10);
            input2[i] = random.nextInt(10);
        }
    }


}
