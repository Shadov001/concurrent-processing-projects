#include <stdlib.h>
#include <stdio.h>
#include "mpi.h" 
#include <math.h>
 
int main(int argc, char **argv) {
	 
	int rank, size, source, tag, length, number, division, from, to,i,k;
	tag = 0;
	length = 20;
	number = 10;
	division = 0;
	from = 0;
	to = 0;
	k = 1;

	double  summ,sum;
	summ = 0;
	sum = 0;
	char name[20];

	MPI_Status status; 
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&rank); 
	MPI_Comm_size(MPI_COMM_WORLD,&size); 
	MPI_Get_processor_name(name,&length);

	if(rank==0) {
		printf("-----------------\n ");
		printf("Give me a number: ");
		scanf ("%d", &number);
		printf("\n-----------------\n ");
	}

	MPI_Bcast(&number,1, MPI_INT, 0, MPI_COMM_WORLD);

	if ((number%size)==0) {
		division = (number / size);
	}
	else {
		division = (number / size) + 1;
	}

	from=division*rank;
	to=from+division-1;

	if (to>number) {
		to = number;
	}
	printf("\n PROCESS number %d have range from %d to %d \n", rank,from,to);

	for(i=from; i <= to;i++) {
		k=i;
		if(k==0) continue;
			sum+=1.0/k;
	}
	if(rank!=0) {
		MPI_Send(&sum,1,MPI_DOUBLE,0,tag,MPI_COMM_WORLD);
	}
	else {
		for (i=1; i<=size-1; ++i) {
			MPI_Recv(&summ,1,MPI_DOUBLE,i,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			sum+=summ;
		}
		printf("-----------------\n ");
		printf("\n Result : %lf\n",sum-log(number));
		printf("-----------------\n ");

	}
	MPI_Finalize(); 
	return(0); 
}



