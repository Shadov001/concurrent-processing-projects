#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#define N 40
#define M 50
#define P 60


void main() {
    int i,j,k;

    double ** macierzA;
    macierzA = (double**)malloc(N*sizeof(double*));
    for(i=0;i<N;i++)
        macierzA[i]=(double*)malloc(M*sizeof(double));

     double ** macierzB;
    macierzB = (double**)malloc(M*sizeof(double*));
    for(i=0;i<M;i++)
        macierzB[i]=(double*)malloc(P*sizeof(double));

    double ** macierzC;
    macierzC = (double**)malloc(N*sizeof(double*));
    for(i=0;i<N;i++)
        macierzC[i]=(double*)malloc(P*sizeof(double));
  
    srand(time(NULL));
    double wtime, wtimep;


    //     printf("A\n");
    // for(i=0;i<N;i++) {
    //             printf("\n");
    //     for(j=0;j<M; j++) {
    //         macierzA[i][j]=rand()%10;
    //                     printf("%.2f    ", macierzA[i][j]);

    //     }
    // }

    //     printf("\n\n B\n");
    // for(i=0;i<M;i++) {
    //             printf("\n");
    //     for(j=0;j<P; j++) {
    //          macierzB[i][j]= rand()%10;
    //                     printf("%.2f    ", macierzB[i][j]);
    //     }
    // }


    wtime = omp_get_wtime();
 #pragma omp parallel for private(i,j,k) num_threads(6) collapse(2)
for(i=0; i<N; i++) {
 //#pragma omp parallel for private(j,k) num_threads(6) 
       for(j=0; j<P; j++) {
           macierzC[i][j]=0;
 //#pragma omp parallel for private(k) num_threads(6) 
           for(k=0;k<M;k++) {
              macierzC[i][j] += macierzA[i][k]*macierzB[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);


 wtime = omp_get_wtime();
 //#pragma omp parallel for private(i,j,k) num_threads(6) 
for(i=0; i<N; i++) {
 #pragma omp parallel for private(j,k) num_threads(6) 
       for(j=0; j<P; j++) {
           macierzC[i][j]=0;
 //#pragma omp parallel for private(k) num_threads(6) 
           for(k=0;k<M;k++) {
              macierzC[i][j] += macierzA[i][k]*macierzB[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);


 wtime = omp_get_wtime();
 //#pragma omp parallel for private(i,j,k) num_threads(6) 
for(i=0; i<N; i++) {
 //#pragma omp parallel for private(j,k) num_threads(6) 
       for(j=0; j<P; j++) {
           macierzC[i][j]=0;
 #pragma omp parallel for private(k) num_threads(6) 
           for(k=0;k<M;k++) {
#pragma omp critical              
macierzC[i][j] += macierzA[i][k]*macierzB[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);



	//printf("\n\n A*B =\n");
        //for(i=0;i<N;i++) {
        //        printf("\n");
        //for(j=0;j<P; j++) {
        //                printf("%.2f    ", macierzC[i][j]);
       // }
   // }


//printf("\n\nCzas: %f\n", wtimep);





}
