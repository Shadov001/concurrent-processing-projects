#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#define N 100
#define M 150
#define P 250


void main() {
    int i,j,k;

    double ** matrix_A;
    matrix_A = (double**)malloc(N*sizeof(double*));
    for(i=0;i<N;i++)
        matrix_A[i]=(double*)malloc(M*sizeof(double));


    double ** matrix_B;
 matrix_B = (double**)malloc(M*sizeof(double*));
    for(i=0;i<M;i++)
     matrix_B[i]=(double*)malloc(P*sizeof(double));



    double ** matrix_C;
    matrix_C = (double**)malloc(N*sizeof(double*));
    for(i=0;i<N;i++)
        matrix_C[i]=(double*)malloc(P*sizeof(double));


    
    srand(time(NULL));
    double wtime, wtimep;


   //     printf("A\n");
    for(i=0;i<N;i++) {
   //             printf("\n");
        for(j=0;j<M; j++) {
            matrix_A[i][j]=rand()%10;
   //                    printf("%.2f    ", matrix_A[i][j]);

     }
    }

  //      printf("\n\n B\n");
   for(i=0;i<M;i++) {
   //             printf("\n");
        for(j=0;j<P; j++) {
             matrix_B[i][j]= rand()%10;
  //                      printf("%.2f    ", matrix_B[i][j]);
        }
    }

//zewnetrzna
    wtime = omp_get_wtime();
 #pragma omp parallel for private(i,j,k) num_threads(6) collapse(2)
for(i=0; i<N; i++) {
       for(j=0; j<P; j++) {
           matrix_C[i][j]=0;
           for(k=0;k<M;k++) {
              matrix_C[i][j] += matrix_A[i][k] matrix_B[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);

//środkowa
 wtime = omp_get_wtime();
for(i=0; i<N; i++) {
 #pragma omp parallel for private(j,k) num_threads(6)
       for(j=0; j<P; j++) {
           matrix_C[i][j]=0;
           for(k=0;k<M;k++) {
              matrix_C[i][j] += matrix_A[i][k] matrix_B[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);

//wewnetrzna
 wtime = omp_get_wtime();
for(i=0; i<N; i++) {
       for(j=0; j<P; j++) {
           matrix_C[i][j]=0;
 #pragma omp parallel for private(k) num_threads(6) 
           for(k=0;k<M;k++) {
#pragma omp critical              
matrix_C[i][j] += matrix_A[i][k] matrix_B[k][j];
           }
       }
    }
wtimep = omp_get_wtime() - wtime;

printf("\n\nCzas: %f\n", wtimep);
}
